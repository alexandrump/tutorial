/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var express = require('express'),
        twig = require('twig'),
        app = express();

app.set('port', process.env.PORT || 3000);

app.set('twig options', {
    strict_variables: false
});


/* Temp vars */
var dias = require('./modules/random.js');

function getWeatherData() {
    return {
        locations: [
            {
                name: 'Portland',
                forecastUrl: 'http://www.wunderground.com/US/OR/Portland.html',
                iconUrl: 'http://icons-ak.wxug.com/i/c/k/cloudy.gif',
                weather: 'Overcast',
                temp: '54.1 F (12.3 C)'
            },
            {
                name: 'Bend',
                forecastUrl: 'http://www.wunderground.com/US/OR/Bend.html',
                iconUrl: 'http://icons-ak.wxug.com/i/c/k/partlycloudy.gif',
                weather: 'Partly Cloudy',
                temp: '55.0 F (12.8 C)'
            },
            {
                name: 'Manzanita',
                forecastUrl: 'http://www.wunderground.com/US/OR/Manzanita.html',
                iconUrl: 'http://icons-ak.wxug.com/i/c/k/rain.gif',
                weather: 'Light Rain',
                temp: '55.0 F (12.8 C)'
            }
        ]
    };
}


/* Dev Enviroment */
app.use(function (req, res, next) {
    res.locals.showTests = app.get('env') !== 'production' && req.query.test === '1';
    next();
});


/* Static Files */
app.use(express.static(__dirname + '/public'));

/* ROUTES */

//Index page 
app.get('/', function (req, res) {
    res.render('home.html.twig', {
        ubicacion: getWeatherData()
    });
});

//About page
app.get('/about', function (req, res) {
    res.render('about.html.twig', {
        dias: dias.getDias(),
        pageTestScript: '/qa/tests-about.js'
    });
});

//Info page 
app.get('/info', function (req, res) {
    res.render('info.html.twig');
});

//Contact page 
app.get('/contact', function (req, res) {
    res.render('contact.html.twig');
});

//show headers
app.get('/headers', function (req, res) {
    res.set('Content-Type', 'text/plain');
    var s = '';
    for (var name in req.headers) {
        s += name + ': ' + req.headers[name] + '\n';
    }
    res.send(s);
});

//Another Webpage
app.get('/tal', function ( req, res){
    res.render('contact.html.twig');
});

//Partials


// custom 404 page
app.use(function (req, res) {
    res.status(404);
    res.render('404.html.twig');
});

// custom 500 page
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500.html.twig');
});

app.listen(app.get('port'), function () {
    console.log('Express iniciado en http://localhost:' +
            app.get('port') + '; pulsa 0Ctrl+C para cerrar el servidor.');
});