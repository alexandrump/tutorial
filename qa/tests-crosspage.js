var Browser = require('zombie'),
        assert = require('chai').assert;
var browser;

suite('Cross-Page Tests', function(){
    
    setup(function(){
        browser = new Browser();
    });
    
//    test('Petición de testeo de INFO', function(done) {
//        var referrer = 'http://localhost:3000/info';
//        browser.visit(referrer, function(){
//            browser.clickLink('.requestGroupRate', function(){
//                assert(browser.field('referrer').value
//                    === referrer);
//                done();
//            });
//        });
//    });
    
//    test('Petición de testeo de NOEXISTE', function(done) {
//        var referrer = 'http://localhost:3000/noexiste';
//        browser.visit(referrer, function(){
//            browser.clickLink('.requestGroupRate', function(){
//                assert(browser.field('referrer').value
//                    === referrer);
//                done();
//            });
//        });
//    });
    
    test('Petición de testeo de la página de contacto', function(done) {
        browser.visit('http://localhost:3000/contact', function(){
            assert(browser.field('referrer').value === '' );
            done();
        });
    });
    
});