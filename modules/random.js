/* Temp vars */
var dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];

/* Variable Global Export*/
exports.getDias = function() {
    var random = Math.floor(Math.random()*dias.length);
    return dias[random];
};